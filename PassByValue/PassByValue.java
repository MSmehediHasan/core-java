package PassByValue;

public class PassByValue {
    int value1 = 50;

    void change(int value1){
        value1 = value1+100;
        System.out.println("fist value of change method " + value1);
    }

    public static void main(String[] args) {

        PassByValue passByValue = new PassByValue();
        System.out.println(passByValue.value1);
        passByValue.change(100);
    }
    

}
