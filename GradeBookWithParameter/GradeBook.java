package GradeBookWithParameter;
//Class declaration with one method that has parameter
public class GradeBook {
    public void displayMassage(String courseName) {
        System.out.printf("Welcome to the grade book for\n%s\n", courseName);
    }
}
