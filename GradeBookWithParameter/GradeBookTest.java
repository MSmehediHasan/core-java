package GradeBookWithParameter;

import java.util.Scanner;

//create a GradeBook object and pass a String its displayMessage method
public class GradeBookTest {
    public static void main(String [] args){
        Scanner input = new Scanner(System.in);
        GradeBook myGradeBook = new GradeBook();
        System.out.println("Please enter your couse name:");
        String nameOfCourse = input.nextLine();
        myGradeBook.displayMassage(nameOfCourse); //call myGradeBook's displayMessage Method
                                                    //and pase nameOfCourse as an argument
    }

}
