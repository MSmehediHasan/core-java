package PascalsTriangle;

import java.util.Scanner;

public class PascalsTriangle {
    public static void printPascal(int n) {
        for (int line = 1; line <= n+1; line++) {

            int C = 1;
            for (int i = 1; i <= line; i++) {

                System.out.print(C + " ");
                C = C * (line-i ) / i;

            }
            System.out.println();
        }
    }


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        printPascal(n);
    }
}
