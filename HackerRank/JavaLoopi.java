import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class JavaLoopi {



    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int N = scanner.nextInt();
       
        int i;
        for(i=1; 1<=i && i<=10; i++){
            
            System.out.println(N + " x " + i + " = " + N*i);
        }

        scanner.close();
    }
}
