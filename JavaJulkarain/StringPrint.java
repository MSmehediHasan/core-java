package JavaJulkarain;

import java.util.Scanner;

public class StringPrint {
    public static void main(String[] args) {
        prln("Enter your name: ");
        Scanner input = new Scanner(System.in);
        String myName = input.nextLine();
        //String myName = new String("ABCD");
        //String yourName = new String("ABCDE");
        // prln(myName);
        //prln(yourName);
        prln("the length of your name is: " + myName.length());
    }

    static void prln(Object anyObject) {
        System.out.println(anyObject);
    }
}
