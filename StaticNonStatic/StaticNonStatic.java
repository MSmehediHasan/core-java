package StaticNonStatic;

public class StaticNonStatic {
    static final long FINAL_VARIABLE = 100*100;
    static int variable;
    //int variable;
    static {
        System.out.println("this is from first static block");
    }
    static {
        System.out.println("this is from second static block:" + FINAL_VARIABLE + "\n this is variable value=" + variable);
    }
    {
        variable++;
        System.out.println("increment variable value" + variable);
    }

}



