package ArgumentNonargumentConstructor;

import java.util.Date;

public class ConstructorWithArgumentAndWithoutArg {
    String value = "this";
    Date date = new Date();
    double d;


    ConstructorWithArgumentAndWithoutArg() {
        System.out.println("from constructor");

    }

    //constructor overloading

    ConstructorWithArgumentAndWithoutArg(String value) {
        this.value = value;
    }

}
