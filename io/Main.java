package io;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        ResultSheet resultSheet = new ResultSheet();

        List<String> list = new ArrayList<>();
        list.add("ID,Name,Bangla,English,Math,Religion");
        list.add("01,Mahadi,60,65,70,75");
        list.add("02,Hasaan,65,70,75,80");


        resultSheet.inputMarks(list);
        resultSheet.retrieveDataFromInput();
        resultSheet.markSheet();
    }
}

