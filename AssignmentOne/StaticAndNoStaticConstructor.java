public class StaticAndNoStaticConstructor {
    static int num1;


    static {
        System.out.println(num1);
    }

    public static void main(String[] args) {
        int num2 = 9;
        System.out.println(num2);
    }
}
