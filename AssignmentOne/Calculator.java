import java.util.Scanner;

public class Calculator {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Do you want to add, subtract, divide, or multiply:");
        String s = sc.nextLine();

        while (s.equals("add") || s.equals("subtract") || s.equals("multiply") || s.equals("divide")) {
            System.out.println("Please input a number:");
            int a = Integer.parseInt(sc.nextLine());
            System.out.println("Please input another number:");
            int b = Integer.parseInt(sc.nextLine());

            if (s.equals("add")) {
                int sum = a + b;
                System.out.println("The sum is: " + sum);
            } else if (s.equals("subtract")) {
                int diff = a - b;
                System.out.println("The difference is: " + diff);
            } else if (s.equals("multiply")) {
                int prod = a * b;
                System.out.println("The product is: " + prod);
            } else if (s.equals("divide")) {
                int div = a / b;
                System.out.println("The quotient is: " + div);
            }

            System.out.println("\nDo you want to add, subtract, divide, or multiply:");
            s = sc.nextLine();
        }

        System.out.println("\nThe end.");
    }

}
