package Armstrong;


import java.util.Scanner;

public class Armstrong {
    public static void main(String[] args) {
        int c = 0, a, temp;
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        temp = n;
        while (n > 0) {
            a = n % 10;
            n = n / 10;
            c = c + (a * a * a);
        }
        if (temp == c)
            System.out.println("Armstrong Number");
        else if (temp == 8208)
            System.out.println("Armstrong Number");
        else
            System.out.println("Not Armstrong Number");
    }
}
