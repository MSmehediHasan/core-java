package Anagram;


import java.util.Arrays;
import java.util.Scanner;
public class Anagram {
    static String isAnagram(String str1, String str2) {
        String s1 = str1.replaceAll("\\s", "");
        String s2 = str2.replaceAll("\\s", "");
        boolean status = true;
        if (s1.length() != s2.length()) {
            status = false;
        } else {
            char[] ArrayS1 = s1.toLowerCase().toCharArray();
            char[] ArrayS2 = s2.toLowerCase().toCharArray();
            Arrays.sort(ArrayS1);
            Arrays.sort(ArrayS2);
            status = Arrays.equals(ArrayS1, ArrayS2);
        }
        if (status) {
            return "Yes";
        } else {
            return "No";
        }
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s1,s2;
        s1 = sc.next();
        s2 = sc.next();
        System.out.printf("%s", isAnagram(s1, s2));
    }
}





