package JavaSeStepByStepLearnCode;

import java.util.Scanner;

public class SwitchCase {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the day number: ");
        int number = scanner.nextInt();
        switch (number){
            case 1:
                System.out.println("The day is Saterday");
                break;
            case 2:
                System.out.println("The day is Sunday");
                break;
            case 3:
                System.out.println("The day is Monday");
                break;
            case 4:
                System.out.println("The day is Tuesday");
                break;
            case 5:
                System.out.println("The day is Wensday");
                break;
            case 6:
                System.out.println("The day is Thusday");
                break;
            case 7:
                System.out.println("The day is Friday");
                break;
            default:
                System.out.println("please enter a valid day number");

        }
    }
}
