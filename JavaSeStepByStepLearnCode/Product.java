package JavaSeStepByStepLearnCode;

import java.util.Scanner;

public class Product {
    public static void main(String args[]){
        Scanner input = new Scanner(System.in);
        int numberOne, numberTwo, numberThree, result;
        System.out.println("Enter your first integer number:");
        numberOne = input.nextInt();
        System.out.println("Enter your second integer number:");
        numberTwo = input.nextInt();
        System.out.println("Enter your third integer number:");
        numberThree = input.nextInt();
        result = numberOne * numberTwo * numberThree;
        System.out.printf("The result is: %d", result);


    }
}
