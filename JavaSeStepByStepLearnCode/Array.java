package JavaSeStepByStepLearnCode;

/*
public class Array {
    public static void main(String [] args) {
        //int array[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
        int[] array = new int [2];
        System.out.println(array[1]);
    }
}

 */
/*
public class Array {
    int array[];
    public static void main(String[] args) {
        Array ad = new Array();
        System.out.println("array was initialized with " + ad.array);
    }
}

 */
public class Array {
    public static void main(String[] args) {
        int[] myNumbers = {1, 2, 3, 4};
        for (int i = 0; i < myNumbers.length; ++i) {
            System.out.println(myNumbers[i]);
        }
    }
}
