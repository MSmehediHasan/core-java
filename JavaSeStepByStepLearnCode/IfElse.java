package JavaSeStepByStepLearnCode;

import java.util.Scanner;


public class IfElse {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.println("Enter the number: ");
        Integer numbers = input.nextInt();
        if (numbers < 10) {
            System.out.println("the number is less than 10");
        } else if (numbers == 10) {
            System.out.println("the number is equal to 10");
        } else {
            System.out.println("the number is gratter than 10");
        }

        /*
        int time = 20;
        String result = (time < 18) ? "Good day." : "Good evening.";
        System.out.println(result);
        */

    }

}

