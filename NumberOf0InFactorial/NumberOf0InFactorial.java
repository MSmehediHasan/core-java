package NumberOf0InFactorial;

import java.util.Scanner;

public class NumberOf0InFactorial {

    public static void main(String[] arg)
    {

        Scanner in = new Scanner(System.in);
        System.out.print("");
        int n = in.nextInt();
        int n1 = n;
        long N = 0;
        while (n != 0)
        {
            N += n / 5;
            n /= 5;
        }

        System.out.println(N);
    }

}