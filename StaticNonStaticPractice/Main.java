package StaticNonStaticPractice;

import static StaticNonStaticPractice.StaticTest.test;

public class Main {
    public static void main(String[] args) {
        NonStatic noneStatic = new NonStatic();
        System.out.println("the value of nonstatic: " + noneStatic.j);
        System.out.println("the value ofstatic: "+ test());
    }
}
